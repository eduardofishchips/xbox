<?php

namespace App\Providers;

use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        'Illuminate\auth\Events\Logout' => [
            'App\Listeners\DeleteSessionData',
        ],
        'App\Events\EmailGanadorUsuario' => [
            'App\Listeners\EnviarCorreoGanador'
        ],
        'App\Events\EmailPerdedorUsuario' => [
            'App\Listeners\EnviarCorreoPerdedor'
        ],
        //'Illuminate\Auth\Events\Login' => [
        //    'App\Listeners\SuccessfulLogin'
        // ],
    ];

    /**
     * Register any events for your application.
     *ss
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
