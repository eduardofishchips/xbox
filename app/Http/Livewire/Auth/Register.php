<?php

namespace App\Http\Livewire\Auth;

use App\Providers\RouteServiceProvider;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Auth\Events\Registered;
use Carbon\Carbon;
use Livewire\Component;

class Register extends Component
{
    public $nombre, $apellido, $ciudad, $codigo_postal, $telefono, $email, $password, $confirm_password, $fecha_nacimiento;
    public $terminos, $promos = false;

    /** Validaciones en tiempo real */
    public function updatedEmail()
    {
        $this->validate([
            'email' => ['email','unique:users']
        ]);
    }

    public function updatedCodigoPostal()
    {
        $this->validate([ 'codigo_postal' => 'digits:5']);
    }

    public function updatedTelefono()
    {
        $this->validate(['telefono' => 'digits:10']);
    }

    /** Registar usuario */
    public function register()
    {
        $this->validate([
            'nombre' => ['required'],
            'apellido' => ['required'],
            'ciudad' => ['required'],
            'codigo_postal' => ['required', 'digits:5'],
            'email' => ['required', 'email', 'unique:users'],
            'password' => ['required', 'min:8', 'same:confirm_password'],
            'terminos' => ['accepted'],
            'fecha_nacimiento' => ['required','date_format:d/m/Y', 'nullable']
        ]);

        $user = User::create([
            'name' => $this->nombre,
            'lastname' => $this->apellido,
            'birth_date' => Carbon::createFromFormat('d/m/Y', $this->fecha_nacimiento),
            'phone_number' => $this->telefono,
            'city' => $this->ciudad,
            'postal_code' => $this->codigo_postal,
            'terms' => $this->terminos,
            'promos' => $this->promos,
            'email' => $this->email,
            'password' => Hash::make($this->password),
        ]);


        event(new Registered($user));
        Auth::login($user, true);

        redirect()->intended(route('tickets'));
    }

    public function render()
    {
        return view('livewire.auth.register');
    }
}
