<?php

namespace App\Http\Livewire;

use Livewire\Component;

class ContadorPremios extends Component
{
    public $premios;

    public function mount()
    {
        $this->premios = contador_premios();
    }

    public function render()
    {
        return view('livewire.contador-premios');
    }
}
