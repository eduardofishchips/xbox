<?php

namespace App\Http\Livewire\Trivias;

use Livewire\Component;

class GameOverUltimo extends Component
{
    public function render()
    {
        return view('livewire.trivias.game-over-ultimo');
    }
}
