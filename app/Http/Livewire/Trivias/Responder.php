<?php

namespace App\Http\Livewire\Trivias;

use Livewire\Component;
use App\Events\EmailGanadorUsuario;
use App\Events\EmailPerdedorUsuario;

use App\Trivia;
use App\WeeklyReward;

class Responder extends Component
{
    public $trivias;
    public $trivia_actual;
    public $numero_preguntas = 2;
    public $pregunta_actual = 1;
    public $nombre_nivel;
    public $duracion_gamepass;
    public $email;
    public $nivel;
    public $mensaje_flash;
    public $mostrar_siguiente_nivel = false;
    public $reclamar_premio = false;
    public $reclamado = false;
    public $ultima = false;
    public $subir;
    public $mostrar_no_premios = false;
    public $mostrar_boton_siguiente = true;

    protected $listeners = ['respuestaTrivia' => 'respuestaTrivia'];

    public function reclamar_premio()
    {
        $this->mostrar_siguiente_nivel = false;
        $this->reclamar_premio = true;
    }

    public function mount()
    {
        $this->email = auth()->user()->email;
        $this->nivel = session('nivel');
        $this->nombreNivel();

        if($this->nivel < 4)
        {
            $this->trivias = obtener_trivias($this->nivel, $this->numero_preguntas);
            $this->trivia_actual = $this->trivias[$this->pregunta_actual-1];
        }
    }

    public function respuestaTrivia($respuesta)
    {
        if(!$respuesta)
        {
            $this->ultima = true;
            invalidar_ticket(session('ticket'));
            event(new EmailPerdedorUsuario($this->nivel));
            sleep(2);
            session()->flash('titulo', 'Game Over');
            session()->flash('message', 'Registra un nuevo ticket para seguir participando');
            redirect()->intended(route('game-over'));
        }
        else
        {
            $this->siguientePregunta();
        }
    }

    public function siguientePregunta()
    {
        $this->pregunta_actual++;
        if( $this->pregunta_actual <= $this->numero_preguntas)
        {
            $this->trivia_actual = $this->trivias[$this->pregunta_actual-1];
            $this->emit('nuevaPregunta', $this->trivia_actual, $this->ultima);
        }
        else
        {
            //Buscar si hay premios en los siguientes niveles
            $proximo_nivel = obtener_siguiente_nivel($this->nivel);
            if(!$proximo_nivel)
            {
                $this->mostrar_boton_siguiente = false;
            }


            $this->ultima = true;
            $this->pregunta_actual--;
            $this->mostrar_siguiente_nivel = true;
        }
    }

    public function pasarNivel()
    {
        $subir = subir_nivel(session('ticket'));
        if(!$subir)
        {
            $this->mostrar_no_premios = true;
            invalidar_ticket(session('ticket'));
        }
        else
        {
            session()->flash('titulo', 'Bienvenido al nivel ' . session('nivel'));
            session()->flash('message', 'Responde correctamente');
            redirect()->intended(route('trivias'));
        }
    }

    public function guardarXbox()
    {
        if(registrar_ganador())
        {
            event(new EmailGanadorUsuario($this->nivel, $this->email)); //nivel, premio nombre
            invalidar_ticket(session('ticket'));
            $this->reclamado = true;

            session()->flash('titulo', 'Felicidades');
            session()->flash('message', $this->mensaje_flash);
        }
    }

    public function guardarPremio()
    {
        registrar_ganador();
        $this->reclamado = true;
        event(new EmailGanadorUsuario($this->nivel, $this->email)); //nivel, premio nombre
        invalidar_ticket(session('ticket'));
        session()->flash('titulo', 'Felicidades');
        session()->flash('message', $this->mensaje_flash);
    }

    public function render()
    {
        return view('livewire.trivias.responder');
    }

    private function nombreNivel()
    {
        switch($this->nivel)
        {
            case 1: $this->nombre_nivel = 'básico'; $this->duracion_gamepass = "14 días"; $this->mensaje_flash = "Haz ganado un gamepass de 14 días"; break;
            case 2: $this->nombre_nivel = 'medio'; $this->duracion_gamepass = "1 mes"; $this->mensaje_flash = "Haz ganado un gamepass de 1 mes"; break;
            case 3: $this->nombre_nivel = 'pro'; $this->duracion_gamepass = "3 meses"; $this->mensaje_flash = "Haz ganado un gamepass de 3 meses"; break;
            case 4: $this->nombre_nivel = 'master'; $this->mensaje_flash = "Haz ganado xbox one x"; break;
        }

    }
}
