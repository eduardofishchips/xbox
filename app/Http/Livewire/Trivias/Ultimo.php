<?php

namespace App\Http\Livewire\Trivias;

use Livewire\Component;
use App\Trivia;

use App\Events\EmailGanadorUsuario;
use App\Events\EmailPerdedorUsuario;

class Ultimo extends Component
{
    public $comenzar = false;
    public $trivias;
    public $trivia_actual;
    public $numero_preguntas = 2;
    public $pregunta_actual = 1;
    public $email;
    public $nivel;
    public $mensaje_flash;
    public $mostrar_ganador = false;
    public $ultima = false;

    protected $listeners = ['respuestaTrivia' => 'respuestaTrivia'];

    public function mount()
    {
        $this->email = auth()->user()->email;
        $this->nivel = session('nivel');
        $this->trivias = obtener_trivias($this->nivel, $this->numero_preguntas);
        $this->trivia_actual = $this->trivias[$this->pregunta_actual-1];
    }

    public function comenzarTrivia()
    {
        $this->comenzar = true;
    }

    public function respuestaTrivia($respuesta)
    {
        if(!$respuesta)
        {
            $this->perder();
        }
        else
        {
            $this->siguientePregunta();
        }
    }

    public function siguientePregunta()
    {
        $this->pregunta_actual++;
        if( $this->pregunta_actual <= $this->numero_preguntas)
        {
            if($this->pregunta_actual == $this->numero_preguntas)
            {
                $this->ultima = true;
            }

            $this->trivia_actual = $this->trivias[$this->pregunta_actual-1];
            $this->emit('nuevaPregunta', $this->trivia_actual, $this->pregunta_actual, $this->ultima);
        }
        else
        {
            $this->pregunta_actual--;
            $this->mostrar_ganador = true;
        }
    }

    public function perder()
    {
        invalidar_ticket(session('ticket'));
        event(new EmailPerdedorUsuario($this->nivel));
        sleep(2);
        session()->flash('titulo', 'Game Over');
        session()->flash('message', 'Registra un nuevo ticket para seguir participando');
        redirect()->intended(route('game-over-ultimo'));
    }

    public function guardarXbox()
    {
        if(registrar_ganador())
        {
            event(new EmailGanadorUsuario($this->nivel, $this->email)); //nivel, premio nombre
            invalidar_ticket(session('ticket'));
            session()->flash('titulo', 'Felicidades!!!');
            session()->flash('message', 'Haz ganado un xbox one x');
            redirect()->intended(route('tickets'));
        }
    }

    public function render()
    {
        return view('livewire.trivias.ultimo');
    }
}
