<?php

namespace App\Http\Livewire\Trivias;

use Livewire\Component;
use App\Trivia;

class Pregunta extends Component
{

    public $trivia;
    public $trivia_id;
    public $pregunta;
    public $tiempo_limite;
    public $respuestas;
    public $respuesta = 0;
    public $respuesta_texto = 'Tiempo agotado';
    public $respuesta_incorrecta ;
    public $tiempo_agotado;
    public $ultima = false;

    public $log;

    protected $listeners = ['nuevaPregunta'];

    public function responderTrivia($respuesta)
    {
        $this->respuesta = $respuesta;
        $this->respuesta_texto = $this->respuestas[$respuesta];

        //Obtener la respuesta correcta de la base de datos
        $respuesta_correcta = Trivia::select('correct')->where('id', $this->trivia)->first();
        if($respuesta_correcta->correct == $this->respuesta)
        {
            $this->emitir_respuesta(true);
        }
        else
        {

            $this->respuesta_incorrecta = true;
            $this->emitir_respuesta(false);
        }
    }

    public function timeOver()
    {
        $this->tiempo_agotado = true;
        $this->emitir_respuesta(false);
    }

    public function nuevaPregunta($trivia, $ultima)
    {
        $this->reset();
        $this->ultima = $ultima;
        $this->trivia = $trivia['id'];
        $this->pregunta = $trivia['question'];
        $this->tiempo_limite = $trivia['time_limit'];
        $this->respuestas = json_decode($trivia['answers'],true);

        $this->log = iniciar_log_trivia($this->trivia, $this->pregunta);
    }

    public function emitir_respuesta($correcta)
    {
        terminar_log_trivia($this->log, $this->respuesta, $this->respuesta_texto, $correcta);
        $this->emitUp('respuestaTrivia', $correcta);
    }

    public function mount($trivia, $ultima)
    {
        $this->trivia = $trivia['id'];
        $this->pregunta = $trivia['question'];
        $this->tiempo_limite = $trivia['time_limit'];
        $this->respuestas = json_decode($trivia['answers'],true);

        $this->log = iniciar_log_trivia($this->trivia, $this->pregunta);
    }

    public function render()
    {
        return view('livewire.trivias.pregunta');
    }
}
