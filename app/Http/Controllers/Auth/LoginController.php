<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Laravel\Socialite\Facades\Socialite;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Illuminate\Http\Request;

use App\User;
use App\SocialAccount;
use Carbon\Carbon;

class LoginController extends Controller
{
    /**
     * Redirect the user to the GitHub authentication page.
     *
     * @return \Illuminate\Http\Response
     */
    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback($provider, Request $request)
    {
        if(! $request->has('code') | $request->has('denied'))
        {
            return redirect()->intended(route('login'));
        }

        $social_user = Socialite::driver($provider)->user();
        $social_account = SocialAccount::whereProvider($provider)->whereProviderUserId($social_user->getId())->first();
        if($social_account)
        {
            return $this->authAndRedirect($social_account->user);
        }

        $user = User::whereEmail($social_user->email)->first();

        if(!$user)
        {
            $user = User::create([
                'name' => $social_user->getName(),
                'email' => $social_user->getEmail(),
                'password' => Hash::make(Str::random(8)),
                'avatar' => $social_user->getAvatar()
            ]);

            $user->markEmailAsVerified();
        }


        $social_account = SocialAccount::create([
            'user_id' => $user->id,
            'provider_user_id' => $social_user->id,
            'provider' => $provider
        ]);

        return $this->authAndRedirect($user);
    }

    public function authAndRedirect($user)
    {
        Auth::login($user);

        $redirect = obtener_ruta_login();

        session()->flash('titulo', 'Bienvenido');
        session()->flash('message', auth()->user()->name);

        if($redirect == 'tickets')
            return redirect()->intended(route('tickets'));
        else
            return redirect()->intended(route('trivias'));

        //redirect()->intended(route('tickets'));
    }
}
