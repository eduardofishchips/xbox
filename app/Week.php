<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Week extends Model
{
    public function tickets()
    {
    	return $this->hasMany('App\Ticket');
    }

    public function weekly_rewards()
    {
    	return $this->hasMany('App\WeeklyReward');
    }
}
