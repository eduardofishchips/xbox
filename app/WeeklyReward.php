<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WeeklyReward extends Model
{
    public function week()
    {
    	return $this->belongsTo('App\Week');
    }

    public function reward()
    {
    	return $this->belongsTo('App\Reward');
    }
}
