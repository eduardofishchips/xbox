<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Notifications\EmailVerificacionRegistro;
use App\Notifications\EmailResetPassword;

use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'lastname', 'birth_date', 'phone_number', 'city', 'postal_code', 'terms', 'promos', 'email', 'password', 'avatar'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $dates = [
        'deleted_at'
    ];

    /**
     * Relacion con la tabla de Tickets
     */
    public function tickets()
    {
        return $this->hasMany('App\Ticket');
    }

    public function sendEmailVerificationNotification()
    {
        $this->notify(new EmailVerificacionRegistro());
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify( new EmailResetPassword($token));
    }
}

