<?php

namespace App\Listeners;

use Illuminate\Auth\Events\Login;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Ticket;

class SuccessfulLogin
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Login  $event
     * @return void
     */
    public function handle(Login $event)
    {
        //Obtener semana
        $week = current_week();
        //Id del usuario logueado
        $user_id = $event->user->id;
        //Buscar si tiene un ticket activo o no
        $ticket = Ticket::where('user_id', $user_id)
                        ->where('is_valid', 1)
                        ->where('week_id', $week)
                        ->first();

        if(!$ticket)
        {
            return redirect()->intended(route('tickets'));
        }

        return redirect()->intended(route('trivias', compact($ticket)));
    }
}
