<div id="glitch" class="fixed top-0 left-0 w-full h-full flex items-center justify-center z-20 p-8 md:p-0">
    <div class="glitch w-full h-full absolute opacity-50"></div>
	<div class="bg-black relative z-50 max-w-sm sm:max-w-lg md:max-w-xl lg:max-w-2xl shadow-lg border-2 border-white animate-none">
		<div class="w-full py-4 px-3 ml-auto bg-x_green_lightest border-b-2 border-white">
			<img class="w-4 h-auto ml-auto" src="{{ asset('images/cerrar.png') }}" alt="">
		</div>
		<div class="px-16">
			<div class="mt-8 w-10 md:w-16 mx-auto">
                <img src="{{ asset('images/modal_desbloquear.svg') }}" alt="">
		    </div>
			<div class="mt-8">
                <p class="text-white font-industry text-xl md:text-3xl leading-none">Desbloqueaste el último nivel.</p>
                <p class="text-x_green_lightest font-industry text-xl md:text-3xl leading-none mt-8">
                    solo los mejores lograrán llegar hasta aquí, y únicamente 5 serán los ganadores de este reto
                </p>
            </div>
            <div class="mt-16 pb-8">
                <a href="{{ route('ultimo-nivel') }}" class="block w-48 py-4 text-center text-white font-industry bg-x_green_light mx-auto hover:bg-x_green_lightest">Entrar</a>
            </div>
		</div>
    </div>
</div>
