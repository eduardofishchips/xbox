@props([
    'nivel',
    'reclamado' => false
])

<div
    class="fixed py-24 top-0 left-0 w-full h-full bg-black flex items-center justify-center z-10" style="background-color: rgba(24,102,8, 0.6);">
    <div class="px-6 py-8 bg-white relative z-50 max-w-sm sm:max-w-lg md:max-w-xl lg:max-w-2xl shadow-lg">
        <div class="px-4">
            <div class="mt-8">
                <h1 class="text-2xl md:text-5xl leading-none text-x_green font-industry text-center py-3 bg-x_green_lightest">Felicidades</h1>
                <p class="text-x_green text-2xl md:text-4xl font-industry text-center leading-tight mt-6">
                    Completaste la @if($nivel == 1) primera @elseif($nivel == 2) segunda @else tercera @endif  misión
                </p>
                <img class="mx-auto my-8" src="{{asset('images/gamepass.png')}}" alt="">
            </div>
            @if(!$reclamado)
                <div class="mx-auto max-w-xs mt-6">
                    <x-input.text_alt wire:model.lazy="email" id="email" name="email" placeholder="Correo electrónico *" :error="$errors->first('email')"/>
                </div>
                <div class="mt-8 flex flex-col md:flex-row items-center justify-evenly">
                    <x-input.link_button x-on:click="reclamado = true" wire:click="guardarPremio" class="cursor-pointer w-full md:w-5/12 block py-6 border-x_green_light border-2 text-x_green_light uppercase font-amsi mt-4 md:mt-0 text-center md:text-lg hover:bg-x_green_light hover:text-white transition-all duration-200 ease-in" texto="Reclamar premio"/>
                </div>
            @else
                <div class="mt-8 flex flex-col md:flex-row items-center justify-evenly">
                    <x-input.link_button href="{{ route('tickets') }}" class="cursor-pointer w-full md:w-5/12 block py-6 border-x_green_light border-2 text-x_green_light uppercase font-amsi mt-4 md:mt-0 text-center md:text-lg hover:bg-x_green_light hover:text-white transition-all duration-200 ease-in" texto="Cerrar"/>
                </div>
            @endif
        </div>
    </div>
</div>

