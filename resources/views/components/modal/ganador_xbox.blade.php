<div class="fixed top-0 left-0 w-full h-full flex items-center justify-center z-10 p-8 md:p-0" style="background-color: rgba(24,102,8, 0.6);">
	<div class="bg-black relative z-50 max-w-sm sm:max-w-lg md:max-w-xl lg:max-w-2xl shadow-lg border-2 border-white">
		<div class="w-full py-4 px-3 ml-auto bg-x_green_lightest border-b-2 border-white flex flex-row items-center justify-between">
            <span class="text-white font-industry text-2xl">Felicidades</span>
			<img class="w-4 h-auto ml-auto" src="{{ asset('images/cerrar.png') }}" alt="">
		</div>
		<div class="px-16">
			<div class="mt-8 w-10 md:w-16 mx-auto">
                <img src="{{ asset('images/xbox_premio.svg') }}" alt="">
		    </div>
			<div class="mt-8">
                <p class="text-x_green_lightest font-industry text-xl md:text-3xl leading-none mt-8">
                    Completaste todas las trivias <br class="hidden md:block"> correctamente
                </p>
            </div>
            <div class="mt-8 pb-8">
                <p class="text-white font-industry text-xl md:text-3xl leading-none mt-8">
                    Eres el ganador, confirma tu correo electrónico:
                </p>
                <div class="mt-3">
                    <input class="bg-black py-3 border-b-2 border-x_green text-x_green w-2/3" wire:model="email" class="text-x_green font-arial" placeholder="Correo electrónico" type="text">
                </div>
                <div class="mt-3 py-5">
                    <a wire:click="guardarXbox" class="block w-48 py-4 text-center text-white font-industry bg-x_green_light mx-auto hover:bg-x_green_lightest cursor-pointer">Empezar</a>
                </div>
            </div>
		</div>
	</div>
</div>
