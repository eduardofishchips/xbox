@props([
	'texto'
])
<button {{ $attributes }}>{{ $texto }}</button>