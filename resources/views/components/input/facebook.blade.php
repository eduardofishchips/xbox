@props([
    'texto' => 'Conectar con facebook'
])
<a class="w-full block py-6 text-white uppercase font-amsi mt-6 text-center md:text-lg bg-x_blue hover:bg-blue-700 transition-all duration-200 ease-in flex items-center justify-center" href="/login/facebook">
	<img class="mr-2 w-5 sm:w-8" src="{{ asset('images/facebook.png') }}" alt=""> {{ $texto }}
</a>
