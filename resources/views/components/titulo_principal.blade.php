@props([
    'fish' => false
])
<div class="mt-12 mx-auto w-11/12 max-w-xs sm:max-w-lg md:max-w-xl text-center font-industry relative">
    @if($fish)
    <img class="absolute t-0 l-0 -mt-5 -ml-5 md:-mt-8 md:-ml-8 z-0 w-10 md:w-auto" src="{{ asset('images/x_titulo.svg') }}" alt="">
    @endif
    <div class="py-3 bg-x_green_lightest relative z-20">
        <h1 class="text-2xl sm:text-4xl md:text-5xl leading-none text-x_green">Registra tus tickets</h1>
    </div>
    <p class="text-lg sm:text-2xl md:text-3xl leading-none mt-2 text-white">y compite para ganar uno de</p>
    <p class="text-2xl sm:text-3xl md:text-4xl leading-snug text-x_green_lightest">nuestros 18,000 premios</p>
</div>

