<div
    x-data="{ tiempo_total: {{ $tiempo_limite }} }"
    x-init="window.timer = setInterval(function(){
        if(tiempo_total >= 0) {
            $refs.contador.innerText = (tiempo_total >= 10 ) ?  tiempo_total : '0' + tiempo_total;
            tiempo_total--;
        }
        else
        {
            $refs.time_over.click();
        }
    }, 1000)"
    >
        <div class="pt-4 pb-12 border-dashed border-b-2 border-x_green_lightest mt-6">
            <input x-ref="time_over" type="hidden" wire:click="timeOver">
            <div class="flex flex-col md:flex-row flex-wrap items-center">
                <div class="w-full md:w-1/3">
                    <p class="font-industry text-x_green_lightest md:text-2xl leading-none">
                        Vidas <img class="w-6 inline-block align-baseline ml-5" src="{{ asset('images/corazon.svg') }}" alt="">
                    </p>
                </div>
                <div class="w-full md:w-1/3 border-r-8 border-x_green_lightest">
                    <p class="font-industry text-x_green_lightest md:text-2xl leading-none">Tiempo para <br class="hidden md:block"> responder</p>
                </div>
                <div class="w-full md:w-1/3">
                    <p class="font-industry text-white md:text-2xl leading-none text-right">00:<span x-ref="contador">00</span>:00 s</p>
                </div>
                <div class="w-full mt-6">
                    <p class="font-industry text-x_green_lightest md:text-2xl leading-none text-right">
                        Consola disponible <span class="text-white ml-8">1</span> <img class="w-8 inline-block align-bottom ml-5" src="{{ asset('images/icono_xbox_ultimo.svg') }}" alt="">
                    </p>
                </div>
            </div>
        </div>
        <div class="mt-8">
            <p class="text-white md:text-2xl font-industry">{{ $pregunta_actual }} de {{ $numero_preguntas }}</p>
            <div class="mt-8">
                <p class="text-x_green_lightest md:text-2xl font-industry">{{ $pregunta }}</p>
                <div class="mt-6 flex flex-col items-start">
                    @foreach ($respuestas as $respuesta => $texto)
                        <a
                        wire:click="responderTrivia({{ $respuesta }})"
                        class="ultima_respuesta text-white font-industry md:text-2xl my-2 cursor-pointer hover:text-x_green">{{ $texto }}</a>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

</div>
