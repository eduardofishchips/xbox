<div class="mx-auto max-w-xs sm:max-w-lg mt-16 flex flex-col items-center justify-center">
    <div class="text-white flex items-center justify-center w-full text-2xl sm:text-4xl md:text-5xl">
        <span class="font-industry">Game</span>
        <span class="font-industry ml-6">Over</span>
    </div>
    <img class="mt-8 w-12 sm:w-16 md:w-20 lg:w-24" src="{{ asset('images/game_over.svg') }}" alt="">
    <p class="text-white font-industry text-center leading-none mt-10 text-xl sm:text-3xl lg:text-5xl">
        Ingresa otro ticket para intentarlo de nuevo
    </p>
    <div class="mt-20 w-full max-w-xs md:max-w-sm px-10">
        <x-input.link_button href="{{ route('tickets') }}" class="mx-auto block py-6 text-white font-amsi text-center border-2 border-white" texto="Regresar al home"/>
    </div>

    <script>
        window.clearInterval(window.timer);
    </script>
</div>
