@section('title', 'Regístrate')

<div>
    <x-titulo_principal/>
    <div class="mx-auto max-w-xs md:w-full lg:max-w-sm mt-16">
        <form wire:submit.prevent="register">
            <x-input.text wire:model.lazy="nombre" oninput="this.value=this.value.replace(/[0-9]/g,'');" id="nombre" name="nombre" placeholder="Nombre *" :error="$errors->first('nombre')" autofocus/>
            <x-input.text wire:model.lazy="apellido" oninput="this.value=this.value.replace(/[0-9]/g,'');" id="apellido" name="apellido" placeholder="Apellido *" :error="$errors->first('apellido')"/>
            <x-input.date wire:model="fecha_nacimiento" id="fecha_nacimiento" name="fecha_nacimiento" placeholder="Fecha de nacimiento" label="Fecha de nacimiento DD/MM/YYYY" :error="$errors->first('fecha_nacimiento')"/>
            <x-input.text wire:model.lazy="ciudad" id="ciudad" name="ciudad" placeholder="Ciudad *" :error="$errors->first('ciudad')"/>
            <x-input.text wire:model.lazy="codigo_postal" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" id="codigo_postal" name="codigo_postal" maxlength="5" placeholder="Código postal *" :error="$errors->first('codigo_postal')"/>
            <x-input.text wire:model.lazy="telefono" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" id="telefono" name="telefono" maxlength="10" placeholder="Teléfono (opcional)" :error="$errors->first('telefono')"/>
            <x-input.text wire:model.lazy="email" id="email" name="email" placeholder="Correo electrónico *" :error="$errors->first('email')"/>
            <x-input.password wire:model.lazy="password" id="password" name="password" placeholder="Contraseña *" :error="$errors->first('password')"/>
            <x-input.password wire:model.lazy="confirm_password" id="confirm_password" name="confirm_password" placeholder="Confirmar contraseña" :error="$errors->first('confirm_password')"/>

            <div class="flex flex-col items-start mt-6">
                <x-input.checkbox wire:model.lazy="terminos" texto="He leído y acepto los términos y condiciones de uso *" :error="$errors->first('terminos')"/>
                <x-input.checkbox wire:model.lazy="promos" texto="Acepto recibir información comercial y promociones de Dairy Queen® México vía correo electrónico"/>
            </div>

            <div class="mt-6">
                <p class="text-xs text-white normal-case font-bold text-center">Los campos marcados con * son obligatorios</p>
            </div>

            <div class="mt-12">
                <x-input.button type="submit" class="bg-x_green_lightest w-full py-6 text-x_green uppercase font-amsi md:text-2xl hover:bg-x_green_light hover:text-white transition-all duration-200 ease-in" texto="regístrate aquí"/>
                <x-input.link_button class="w-full block py-6 border-white border-2 text-white uppercase font-amsi mt-6 text-center md:text-2xl hover:bg-white hover:text-x_green transition-all duration-200 ease-in" href="{{ route('home')}}" texto="Cancelar"/>
            </div>

            <div class="mt-12 text-center">
                <p class="text-md md:text-xl font-amsi normal-case text-white">¿Ya eres usuario? <a class="underline" href="{{ route('login') }}">Inicia sesión</a></p>
            </div>
        </form>
    </div>
</div>

