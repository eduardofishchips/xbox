@section('title', 'Registro de usuario')

<div>
    <x-titulo_principal fish="true"/>
    <div class="mt-16 py-4 mx-auto max-w-xs sm:max-w-sm md:max-w-3xl border-x_green_lightest border-t-2 border-b-2 border-dashed">
        <div class="max-w-sm mx-auto py-10 flex flex-col items-center">
            <x-input.link_button
            href="{{ route('register') }}"
            texto="Regístrate aquí"
            class="font-amsi w-full sm:text-xl text-x_green bg-x_green_lightest py-6 px-20 sm:py-6 sm:px-24 hover:bg-x_green_light hover:text-white transition-all duration-200 ease-in relative x-btn" />
            <x-input.facebook />
            <p class="font-amsi md:text-xl mt-16 normal-case text-white">¿Ya eres usuario?, <a class="underline" href="{{ route('login') }}">Inicia sesión</a></p>
        </div>
    </div>
</div>
