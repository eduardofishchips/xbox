@section('title', 'Iniciar sesión')

<div>
    <x-titulo_principal/>
    <div class="mx-auto max-w-xs md:w-full lg:max-w-sm mt-16">
        <form wire:submit.prevent="authenticate" id="form_login">
            <x-input.text wire:model.lazy="email" id="email" name="email" placeholder="Correo electrónico" :error="$errors->first('email')" autofocus/>
            <x-input.password wire:model.lazy="password" id="password" name="password" placeholder="Contraseña" :error="$errors->first('password')"/>

            <div class="text-center mt-16">
                <!--div class="flex items-center">
                    <input wire:model.lazy="remember" id="remember" type="checkbox" class="form-checkbox w-4 h-4 text-indigo-600 transition duration-150 ease-in-out" />
                    <label for="remember" class="block ml-2 text-sm text-gray-900 leading-5">
                        Remember
                    </label>
                </div-->
                <div class="text-base md:text-xl normal-case text-white">
                    <p class="leading-5">¿Olvidaste tu contraseña?</p>
                    <a href="{{ route('password.request') }}" class="underline">
                        Restablecer contraseña
                    </a>
                </div>
            </div>

            <div class="mt-12">
                <x-input.button type="submit" class="bg-x_green_light w-full py-6 text-white uppercase font-amsi md:text-2xl hover:bg-x_green_lightest transition-all duration-200 ease-in" texto="iniciar sesión"/>
                <x-input.facebook texto="iniciar sesión con facebook"/>
                <x-input.link_button class="w-full block py-6 border-white border-2 text-white uppercase font-amsi mt-6 text-center md:text-2xl hover:bg-white hover:text-x_green transition-all duration-200 ease-in" href="{{ route('home')}}" texto="Cancelar"/>
            </div>


        </form>
    </div>
</div>
