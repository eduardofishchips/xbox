@section('title', 'Date la vuelta con xbox - Regístra tu ticket')

<div x-data="{ open: false, showLoader: false }">

    <div class="mt-16 max-w-md md:max-w-xl mx-auto text-center font-industry">
       <div class="py-3">
           <h1 class="text-2xl md:text-5xl leading-none text-white">Para participar tienes que registrar un ticket</h1>
       </div>
   </div>

   <div class="mt-24 max-w-sm mx-auto px-8">
      <x-input.link_button
      texto="Registrar ticket"
      x-on:click="open = true"
      class="cursor-pointer block text-center font-amsi sm:text-xl text-x_green bg-x_green_lightest py-4 sm:py-6 hover:shadow-2xl hover:border-green-600 hover:bg-x_green_light hover:text-white transition-all duration-200 ease-in" />
    </div>

    <div class="mt-24 px-4 mx-auto w-full md:max-w-3xl lg:max-w-5xl flex flex-col md:flex-row items-center justify-between">
        <div class="trivia_img disable p-5">
            <img src="{{ asset('images/candado.png') }}" alt="trivia imagen" class="ml-auto">
            <div class="mt-12 font-industry">
                <h4 class="text-5xl text-x_green_light">Nivel 1</h4>
                <span class="text-2xl text-white">Básico</span>
            </div>
        </div>
        <div class="trivia_img disable p-5 my-10 md:my-0">
            <img src="{{ asset('images/candado.png') }}" alt="trivia imagen" class="ml-auto">
            <div class="mt-12 font-industry">
                <h4 class="text-5xl text-x_green_light">Nivel 2</h4>
                <span class="text-2xl text-white">Medio</span>
            </div>
        </div>
        <div class="trivia_img disable p-5">
            <img src="{{ asset('images/candado.png') }}" alt="trivia imagen" class="ml-auto">
            <div class="mt-12 font-industry">
                <h4 class="text-5xl text-x_green_light">Nivel 3</h4>
                <span class="text-2xl text-white">Pro</span>
            </div>
        </div>
    </div>

    @livewire('trivias.concursantes');

    @livewire('contador-premios')

    <!-- Modal registro de tickets-->
    <div
    x-show="open"
    x-transition:enter="transition ease-in duration-300"
    x-transition:enter-start="opacity-0 scale-90"
    x-transition:enter-end="opacity-100 scale-100"
    >
        <div class="fixed inset-0 w-full h-full z-10" style="background-color: rgba(24,102,8, 0.6);">
            <div class="py-10 w-full h-full overflow-y-auto flex items-center justify-center">
                <div class="contenedor-modal mt-64 sm:mt-0 px-4 py-8 bg-white relative z-50 max-w-xs sm:max-w-md md:max-w-xl lg:max-w-2xl shadow-lg">


                    <div wire:click="resetForm" class="w-4 h-4 ml-auto cursor-pointer" x-on:click="open=false">
                        <img class="w-full h-auto" src="{{ asset('images/cerrar.png') }}" alt="">
                    </div>

                    <div class="md:px-8">

                        <div class="mt-8">
                            <h1 class="text-xl md:text-3xl lg:text-5xl leading-none text-x_green font-industry text-center py-3 bg-x_green_lightest">Registra</h1>
                            <p class="text-x_green sm:text-2xl md:text-3xl lg:text-5xl font-industry text-center leading-tight mt-2">Tu ticket para poder participar</p>
                        </div>

                        <div class="mt-8">
                            <form wire:submit.prevent="registrar">
                                <div class="sm:px-6 md:px-12 lg:px-16 relative">
                                    <div class="tooltip absolute right-0 mt-6 sm:mr-6 md:mr-12 lg:mr-16 cursor-pointer">
                                        <div class="w-4 h-4 md:w-5 md:h-5" data-tooltip="Tambien lo puedes encontrar como: FOLIO, FACTURA DE VENTA, COMPROBANTE DE PAGO" >
                                            <img src="{{ asset('images/informacion.png') }}" alt="">
                                        </div>
                                    </div>

                                    <x-input.text_alt  wire:model.lazy="ticket" id="ticket" name="ticket" placeholder="Número de ticket" :error="$errors->first('ticket')" autofocus/>

                                    <div class="pb-2 mt-2 relative">
                                        <select wire:model="estado" id="estado" class="appearance-none text-x_green_light text-lg md:text-2xl w-full outline-none border-b-2 border-x_green_light">
                                            <option value="0" selected>Selecciona un estado</option>
                                            @foreach($estados as $id => $nombre)
                                            <option value="{{ $id }}">{{ $nombre }}</option>
                                            @endforeach
                                        </select>
                                        <div class="pointer-events-none absolute h-5 w-5 inset-y-0 right-0 flex items-center text-x_green_light mt-2">
                                            <svg class="fill-current h-5 w-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/></svg>
                                        </div>
                                        <div>
                                            @if($errors->has('estado'))
                                            <p class="error_msg mt-2 text-xs text-red-400 normal-case font-semibold">{{ $errors->first('estado') }}</p>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="pb-2 mt-2 relative @if(!$ciudad_activo || empty($ciudades)) pointer-events-none @endif">
                                        <!--img src="{{ asset('images/cargador.png') }}" alt=""-->
                                        <select wire:model="ciudad" id="ciudad" class="appearance-none text-lg md:text-2xl w-full outline-none border-b-2 @if(!$ciudad_activo || empty($ciudades)) pointer-events-none text-gray-500 border-gray-500 @else text-x_green_light border-x_green_light @endif">
                                            <option value="0" selected>Selecciona una ciudad</option>
                                            @if(!empty($ciudades))
                                                @foreach($ciudades as $id => $nombre)
                                                <option value="{{ $id }}">{{ $nombre }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                        <div class="pointer-events-none absolute h-5 w-5 inset-y-0 right-0 flex items-center mt-2 @if(!$ciudad_activo || empty($ciudades)) text-gray-500 @else text-x_green_light @endif">
                                            <svg class="fill-current h-5 w-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/></svg>
                                        </div>
                                        <div>
                                            @if($errors->has('ciudad'))
                                            <p class="error_msg mt-2 text-xs text-red-400 normal-case font-semibold">{{ $errors->first('ciudad') }}</p>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="pb-2 mt-2 relative @if(!$sucursal_activo || empty($sucursales)) pointer-events-none @endif">
                                        <select wire:model="sucursal" id="sucursal" class="appearance-none text-lg md:text-2xl w-full outline-none border-b-2 @if(!$sucursal_activo || empty($sucursales)) pointer-events-none text-gray-500 border-gray-500 @else text-x_green_light border-x_green_light @endif">
                                            <option value="0" selected>Selecciona una sucursal</option>
                                            @if(!empty($sucursales))
                                                @foreach($sucursales as $id => $nombre)
                                                <option value="{{ $id }}">{{ $nombre }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                        <div class="pointer-events-none absolute h-5 w-5 inset-y-0 right-0 flex items-center mt-2 @if(!$sucursal_activo || empty($sucursales)) text-gray-500 @else text-x_green_light @endif">
                                            <svg class="fill-current h-5 w-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/></svg>
                                        </div>
                                        <div>
                                            @if($errors->has('sucursal'))
                                            <p class="error_msg mt-2 text-xs text-red-400 normal-case font-semibold">{{ $errors->first('sucursal') }}</p>
                                            @endif
                                        </div>
                                    </div>

                                </div>
                                <p class="text-x_green_light font-arial text-xs md:text-base lg:text-2xl normal-case mt-10">
                                    <span class="uppercase font-bold">Atención:</span> Por favor toma una fotografía de cada uno de los tickets que registres, ya que si resultas
                                    ganador será necesaria para reclamar tu premio.
                                </p>

                                <div class="mt-8 flex flex-col md:flex-row items-center justify-evenly">
                                    <x-input.button type="submit" x-on:click="showLoader=true" class="bg-x_green_lightest w-full md:w-5/12 py-6 text-x_green uppercase font-amsi md:text-lg hover:bg-x_green_light hover:text-white transition-all duration-200 ease-in" texto="registrar ticket"/>
                                    <x-input.link_button x-on:click="open=false" wire:click="resetForm" class="cursor-pointer w-full md:w-5/12 block py-6 border-x_green_light border-2 text-x_green_light uppercase font-amsi mt-4 md:mt-0 text-center md:text-lg hover:bg-white hover:text-x_green transition-all duration-200 ease-in" texto="Cancelar"/>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal ya hay ticket -->
    @if($ticket_activo)
    <div>
        <div class="fixed py-24 top-0 left-0 w-full h-full bg-black flex items-center justify-center z-10" style="background-color: rgba(24,102,8, 0.6);">
            <div class="px-6 py-8 bg-white relative z-50 max-w-sm sm:max-w-lg md:max-w-xl lg:max-w-2xl shadow-lg">
                <div class="px-16">
                    <div class="mt-8">
                        <h1 class="text-2xl md:text-5xl leading-none text-x_green font-industry text-center py-3 bg-x_green_lightest">Atencion</h1>
                        <p class="text-x_green text-2xl md:text-5xl font-industry text-center leading-tight mt-6">Tienes un ticket activo</p>
                    </div>
                    <p class="text-x_green_light font-arial text-lg md:text-2xl normal-case mt-10 text-center">
                        Dirígete a la trivia para seguir participando
                    </p>
                    <div class="mt-8 flex flex-col md:flex-row items-center justify-evenly">
                        <x-input.link_button href="{{ route('trivias') }}" class="cursor-pointer w-full md:w-5/12 block py-6 border-x_green_light border-2 text-x_green_light uppercase font-amsi mt-4 md:mt-0 text-center md:text-lg hover:bg-x_green_light hover:text-white transition-all duration-200 ease-in" texto="Ir a la trivia"/>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif


</div>
