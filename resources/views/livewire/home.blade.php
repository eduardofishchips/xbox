@section('title', 'Date la vuelta con xbox')

<div>
    <x-titulo_principal fish=true/>

    <div class="mt-16 mx-auto w-full max-w-sm md:max-w-lg lg:max-w-3xl border-b-2 border-t-2 border-dashed border-x_green_lightest">
        <div class="flex flex-col md:flex-row items-center justify-center w-full sm:w-11/12 lg:w-9/12 py-16 mx-auto" style="background-image: radial-gradient(ellipse at 50% 50%, #186608, #000000 75%);">
            <div class="xbox_reward">
                <img class="w-2/5 mx-auto" src="{{ asset('images/logo_xbox_alt.svg') }}" alt="">
                <img class="mt-4" src="{{ asset('images/premio_xbox.png') }}" alt="" srcset="">
            </div>
        </div>

        <div class="text-center mt-16 py-10">
            <x-input.link_button class="relative z-10 inline-block mx-auto font-amsi text-black bg-x_green_lightest py-4 px-12 md:py-6 md:px-16 hover:bg-x_green_light hover:text-white transition-all duration-200 ease-in x-btn" href="/registro" texto="Regístrate aquí"/>
            <p class="font-amsi md:text-base mt-10 normal-case text-white">¿Ya eres usuario?, <a class="underline" href="{{ route('login') }}">Inicia sesión</a></p>
        </div><!-- Links -->

    </div><!-- Premios -->


    <div class="w-10/12 md:w-11/12 mx-auto text-center text-lg md:text-xl lg:text-2xl leading-tight font-industry text-white mt-16">
        <p>Responde las trivias y avanza de nivel, tú decides el premio <br class="hidden md:block"/>final ¡Sólo los mejores podrán conseguir<br class="hidden md:block"/> una consola xbox one x!</p>
    </div><!-- Dinamica -->

    @livewire('contador-premios')

</div>
