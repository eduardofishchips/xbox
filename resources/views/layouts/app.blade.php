@extends('layouts.base')

@section('body')
	@include('templates/header')

	<main class="main-content" id="main-content">
        @yield('content')
	</main>

    @include('templates/footer')
@endsection
