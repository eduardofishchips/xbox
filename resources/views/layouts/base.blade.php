<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
         <!-- Global site tag (gtag.js) - Google Analytics -->
         <script async src="https://www.googletagmanager.com/gtag/js?id=UA-175559474-1"></script>
         <script>
             window.dataLayer = window.dataLayer || [];
             function gtag(){dataLayer.push(arguments);}
             gtag('js', new Date());

             gtag('config', 'UA-175559474-1');
         </script>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">

        @hasSection('title')

            <title>@yield('title')</title>
        @else
            <title>{{ config('app.name') }}</title>
        @endif

        <!-- Favicon -->
        <link rel="shortcut icon" href="{{ url(asset('favicon.ico')) }}">
        <link rel="icon" type="image/png" href="{{ url('favicon-32x32.png') }}" sizes="32x32" />
        <link rel="icon" type="image/png" href="{{ url('favicon-16x16.png') }}" sizes="16x16" />

        <!-- Scripts -->

        <script defer src="{{ url(mix('js/app.js')) }}"></script>

        <!-- Styles -->
        <link rel="stylesheet" href="{{ url(mix('css/app.css')) }}" data-turbolinks-track="true">

        @livewireStyles

        <link rel="stylesheet" type="text/css" href="{{ asset('css/pikaday.css') }}">
        <link rel="stylesheet" href="{{ asset('css/xbox-calendar.css') }}">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

    </head>
    <body @if(Request::is('registro')) class="registro" @endif
        @if(Request::is('/') || Request::is('trivias/responder') || Request::is('trivias/gameover') || Request::is('trivias/ultimo') || Request::is('trivias/gameover-ultimo')) class="dark" @endif >

        @yield('body')
        @livewireScripts

        <script src="{{ asset('js/moment.min.js') }}"></script>
        <script src="{{ asset('js/pikaday.js') }}"></script>

        <script>
            window.livewire.on('clearForm', ()=> {
                document.querySelectorAll('.error_msg').forEach(e => e.remove());
            })
        </script>
    </body>
</html>
