<?php

use Illuminate\Support\Facades\Route;


use Illuminate\Support\Facades\Notification;
use App\Notifications\NewMessage;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::livewire('/', 'home')->name('home');


Route::get('/login/{provider}', 'Auth\LoginController@redirectToProvider')->name('facebook');
Route::get('/login/{provider}/callback', 'Auth\LoginController@handleProviderCallback')->name('facebook_callback');

Route::livewire('faqs', 'faqs');
Route::livewire('terminos', 'terminos');
Route::livewire('privacidad', 'politica-privacidad');


Route::layout('layouts.auth')->group(function () {
    Route::middleware('guest')->group(function () {
        Route::livewire('login', 'auth.login')
            ->name('login');

        Route::livewire('registro', 'auth.registro')
            ->name('registro');

        Route::livewire('registro-usuario', 'auth.register')
            ->name('register');
    });

    Route::livewire('password/reset', 'auth.passwords.email')
        ->name('password.request');

    Route::livewire('password/reset/{token}', 'auth.passwords.reset')
        ->name('password.reset');

    Route::livewire('email/verify', 'auth.verify')
        ->middleware('throttle:6,1')
        ->name('verification.notice');

    Route::middleware('auth', 'verified')->group(function () {

        Route::livewire('password/confirm', 'auth.passwords.confirm')
            ->name('password.confirm');

        Route::livewire('perfil', 'auth.user.profile')->name('perfil');
        Route::livewire('perfil/editar', 'auth.user.edit')->name('editar_perfil');
        Route::livewire('tickets', 'tickets')->name('tickets');
        Route::livewire('trivias', 'trivias.principal')->name('trivias');
        Route::livewire('trivias/responder', 'trivias.responder')->name('responder_trivia');
        Route::livewire('trivias/gameover', 'trivias.game-over')->name('game-over');
        Route::livewire('trivias/ultimo', 'trivias.ultimo')->name('ultimo-nivel');
        Route::livewire('trivias/gameover-ultimo', 'trivias.game-over-ultimo')->name('game-over-ultimo');
    });
});

Route::middleware('auth')->group(function () {
    Route::get('email/verify/{id}/{hash}', 'Auth\EmailVerificationController')
        ->middleware('signed')
        ->name('verification.verify');

    Route::post('logout', 'Auth\LogoutController')
        ->name('logout');
});
