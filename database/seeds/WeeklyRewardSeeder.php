<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class WeeklyRewardSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $premios_semana = [
        	"1" => [
        		"1" => 2500,
        		"2" => 416,
        		"3" => 166,
        		"4" => 1,
        	],
        	"2" => [
        		"1" => 2500,
        		"2" => 420,
        		"3" => 170,
        		"4" => 0,
        	],
        	"3" => [
        		"1" => 2500,
        		"2" => 416,
        		"3" => 166,
        		"4" => 1,
            ],
            "4" => [
        		"1" => 2500,
        		"2" => 416,
        		"3" => 166,
        		"4" => 1,
            ],
            "5" => [
        		"1" => 2500,
        		"2" => 416,
        		"3" => 166,
        		"4" => 1,
            ],
            "6" => [
        		"1" => 2500,
        		"2" => 416,
        		"3" => 166,
        		"4" => 1,
        	]
        ];

        foreach($premios_semana as $semana => $premios)
        {
        	foreach ($premios as $premio => $cantidad) {
        		DB::table('weekly_rewards')->insert([
	        		'week_id' => $semana,
                    'reward_id' => $premio,
                    'level' => $premio,
	        		'quantity' => $cantidad,
	        		'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
	        		'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
	        	]);
        	}
        }
    }
}
