<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWeeklyRewardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('weekly_rewards', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('week_id');
            $table->integer('level');
            $table->unsignedBigInteger('reward_id');
            $table->integer('quantity');
            $table->timestamps();

            $table->foreign('week_id')->references('id')->on('weeks');
            $table->foreign('reward_id')->references('id')->on('rewards');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('weekly_rewards');
    }
}
